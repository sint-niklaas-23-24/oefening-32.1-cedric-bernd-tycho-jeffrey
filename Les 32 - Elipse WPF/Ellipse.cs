﻿namespace Les_32___Elipse_WPF
{
    internal class Ellipse
    {
        private int _hoogte;
        private int _breedte;
        private double _randdikte;
        //private List<Brush> _colors = new List<Brush>()
        //{
        //    Brushes.Red,
        //    Brushes.Green,
        //    Brushes.Blue
        //};

        public Ellipse(int hoogte, int breedte)
        {
            Hoogte = hoogte;
            Breedte = breedte;
            Randdikte = 0.5;
        }

        public int Hoogte
        {
            get { return _hoogte; }
            set { _hoogte = value; }
        }

        public int Breedte
        {
            get { return _breedte; }
            set { _breedte = value; }
        }
        public double Randdikte
        {
            get { return _randdikte; }
            set { _randdikte = value; }
        }

        //public List<Brush> Colors
        //{
        //    get { return _colors; }
        //    set { _colors = value; }
        //}

    }
}

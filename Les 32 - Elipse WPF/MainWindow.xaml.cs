﻿using System.Collections.Generic;
using System.Windows;

namespace Les_32___Elipse_WPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Ellipse ellipse;
        List<string> kleuren = new List<string>() { "Red", "Green", "Blue" };

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            ellipse = new Ellipse(50, 50);
            cmbColorsBinnen.ItemsSource = kleuren;
            cmbColorsBinnen.SelectedIndex = 0;
            cmbColorsBuiten.ItemsSource = kleuren;
            cmbColorsBuiten.SelectedIndex = 0;
        }
    }
}
